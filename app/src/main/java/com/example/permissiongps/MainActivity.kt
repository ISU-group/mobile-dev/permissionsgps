@file:OptIn(ExperimentalPermissionsApi::class)

package com.example.permissiongps

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.example.permissiongps.ui.theme.PermissionGPSTheme
import com.google.accompanist.permissions.*
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel

@ExperimentalPermissionsApi
class MainActivity : ComponentActivity(), LocationListener {
    val viewModel: LocationViewModel by viewModels()
    lateinit var locationManager: LocationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        setContent {
            PermissionGPSTheme {
                App()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                1000,
                0.01f,
                this
            )
            updateLocation()
        }

    }

    override fun onStart() {
        super.onStart()
        updateLocation()
    }

    private fun updateLocation() {
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {

            val bestProvider = locationManager.getBestProvider(Criteria(), true)
            viewModel.setLocationProviders(locationManager.allProviders)

            if (bestProvider != null && locationManager.isProviderEnabled(bestProvider)) {
                val location = locationManager.getLastKnownLocation(bestProvider)
                if (location != null) onLocationChanged(location)
                Log.d("mytag", "location set")
            } else {
                Log.d("mytag", "bestProvider is null")
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        location.apply {
            viewModel.setLocation(
                longitude = longitude,
                latitude = latitude,
            )
        }

        Log.d("mytag", "Data updated")
    }

    override fun onProviderEnabled(provider: String) {
        Log.d("mytag", "Provider enabled: $provider")
        viewModel.on()
    }

    override fun onProviderDisabled(provider: String) {
        Log.d("mytag", "Provider disabled: $provider")
        viewModel.off()
    }
}

@Composable
fun App(viewModel: LocationViewModel = viewModel()) {
    val permissionState = rememberPermissionState(
        permission = Manifest.permission.ACCESS_FINE_LOCATION
    )

    val state = viewModel.state

    Log.d("mytag", "Screen updated")

    val lifecycleOwner = LocalLifecycleOwner.current
    DisposableEffect(
        key1 = lifecycleOwner,
        effect = {
            val observer = LifecycleEventObserver { _, event ->
                if (event == Lifecycle.Event.ON_START) {
                    permissionState.launchPermissionRequest()
                }
            }

            lifecycleOwner.lifecycle.addObserver(observer)

            onDispose {
                lifecycleOwner.lifecycle.removeObserver(observer)
            }
        }
    )

    Surface {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
        ) {
            when (permissionState.permission) {
                Manifest.permission.ACCESS_FINE_LOCATION -> {
                    when {
                        permissionState.status.isGranted -> {
                            if (state.online) {
                                Text(text = "GPS permission accepted")
                                Text(text = "Latitude: ${state.latitude}")
                                Text(text = "Longitude: ${state.longitude}")

                                if (state.providers.isNotEmpty()) {
                                    LazyColumn {
                                        item {
                                            Text(
                                                text = "Providers:"
                                            )
                                        }

                                        items(state.providers.size) { index ->
                                            Text(text = state.providers[index])
                                        }
                                    }
                                }
                            } else {
                                Text(text = "Offline")
                            }
                        }
                        permissionState.status.shouldShowRationale -> {
                            Text(text = "GPS permission is needed for this application")
                        }
                        permissionState.status.isPermanentlyDenied() -> {
                            Text(text = "GPS permission was permanently denied.")
                            Text(text = "You can enable it in the app settings")
                        }
                    }
                }
            }
        }
    }
}

private fun PermissionStatus.isPermanentlyDenied(): Boolean
    = !isGranted && !shouldShowRationale
