package com.example.permissiongps

data class LocationState(
    var longitude: Double = 0.0,
    var latitude: Double = 0.0,
    var online: Boolean = false,
    var providers: List<String> = listOf()
)
