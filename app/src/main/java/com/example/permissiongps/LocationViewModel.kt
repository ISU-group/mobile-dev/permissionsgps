package com.example.permissiongps

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class LocationViewModel(
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    var state by mutableStateOf(LocationState())
        private set

    fun setLocationProviders(providers: List<String>) {
        state = LocationState(
            longitude = state.longitude,
            latitude = state.latitude,
            online = state.online,
            providers = providers
        )
    }

    fun setLocation(longitude: Double, latitude: Double) {
        state = LocationState(
            longitude = longitude,
            latitude = latitude,
            online = true,
            providers = state.providers
        )
    }

    fun on() = setOnOff(true)

    fun off() = setOnOff(false)

    private fun setOnOff(online: Boolean) {
        state = LocationState(
            longitude = state.longitude,
            latitude = state.latitude,
            online = online,
            providers = state.providers
        )
    }
}